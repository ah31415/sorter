module com.example.sorter {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;

    opens com.uniqueX to javafx.fxml;
    exports com.uniqueX;
    exports com.uniqueX.Controller;
    opens com.uniqueX.Controller to javafx.fxml;
}