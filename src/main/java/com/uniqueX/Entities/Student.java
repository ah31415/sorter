package com.uniqueX.Entities;

public class Student {

    String Name;

    double score;

    public Student(String name, double score) {
        Name = name;
        this.score = score;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "Name='" + Name + '\'' +
                ", score=" + score +
                '}';
    }
}
