package com.uniqueX.Controller;

import com.uniqueX.Entities.Student;
import com.uniqueX.ISorter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

public class HelloController{
    @FXML
    private ComboBox<String> sortType;

    @FXML
    public void initialize() {
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Bubble",
                        "Heap",
                        "Merge"
                );
        sortType.setItems(options);
    }

    @FXML
    public void sortFile() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Open File");
        File file = chooser.showOpenDialog(new Stage());
        Student[] unsortedStudents =readFile(file);

        Instant start = Instant.now();
        Student[] sortedStudent = sortStudent(unsortedStudents,sortType.getValue());
        Instant end = Instant.now();
        Duration timeElapsed = Duration.between(start, end);
        System.out.println("Time to sort: "+ timeElapsed.getNano() +" nanoseconds");
        populateTable(sortedStudent);
    }

    private void populateTable(Student[] sortedStudent) {
        //// TODO: populate table
    }

    @FXML
    public void saveToFile(){
        //// TODO: save to a file
    }

    private Student[] sortStudent(Student[] students, String sortType) {
        ISorter sorter = null;
        try {
            Class c = Class.forName("com.uniqueX.sorter."+sortType);
            sorter= (ISorter) c.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        sorter.sortStudents(students);
        return students;
    }

    private Student[] readFile(File file) {
        BufferedReader objReader = null;
        ArrayList<Student> listOfStudents = new ArrayList<>() ;
        try {
            String strCurrentLine;
            String[] studentArray;
            Student student;
            objReader = new BufferedReader(new FileReader(file));
            while ((strCurrentLine = objReader.readLine()) != null) {
                studentArray=strCurrentLine.split(",");
                student= new Student(studentArray[0],Double.valueOf(studentArray[1]));
                listOfStudents.add(student);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (objReader != null)
                    objReader.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return listOfStudents.toArray(new Student[]{});
    }
}