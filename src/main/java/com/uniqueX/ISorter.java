package com.uniqueX;

import com.uniqueX.Entities.Student;

public interface ISorter {
    void sortStudents(Student[] students);
}
