package com.uniqueX.sorter;

import com.uniqueX.Entities.Student;
import com.uniqueX.ISorter;

import java.util.Arrays;

public class Bubble implements ISorter {

    @Override
    public void sortStudents(Student[] students) {
        System.out.println("Bubble Sort");
        System.out.println(Arrays.toString(students));
        boolean swap =true;
        int i=0;
        while(swap){
            swap=false;
            for (int j = 0; j < students.length - i - 1; j++) {
                if (students[j].getScore() > students[j + 1].getScore()) {
                    Student tempStudent = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = tempStudent;
                    swap=true;
                }
            }
            i++;
        }
        System.out.println(Arrays.toString(students));
    }
}
