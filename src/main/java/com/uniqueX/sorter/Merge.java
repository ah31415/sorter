package com.uniqueX.sorter;

import com.uniqueX.Entities.Student;
import com.uniqueX.ISorter;

import java.util.Arrays;

public class Merge implements ISorter {

    @Override
    public void sortStudents(Student[] students) {
        System.out.println("Merge Sort");
        System.out.println(Arrays.toString(students));
        sort(students);
        System.out.println(Arrays.toString(students));
    }

    private void sort(Student[] all) {
        if (all.length < 2) {
            return;
        }
        int leftSize = all.length / 2;
        int rightSize=all.length - leftSize;
        Student[] leftStudent = new Student[leftSize];
        Student[] rightStudent = new Student[rightSize];

        for (int i = 0; i < leftSize; i++) {
            leftStudent[i] = all[i];
        }
        for (int i = leftSize; i < all.length; i++) {
            rightStudent[i - leftSize] = all[i];
        }
        sort(leftStudent);
        sort(rightStudent);

        merge(all, leftStudent,leftSize, rightStudent, rightSize);
    }

    private void merge(Student[] all,
                      Student[] leftStudent, int leftSize,
                      Student[] rightStudent, int rightSize) {
        int i = 0, j = 0, k = 0;
        while (i < leftSize && j < rightSize) {
            if (leftStudent[i].getScore() <= rightStudent[j].getScore()) {
                all[k++] = leftStudent[i++];
            }else {
                all[k++] = rightStudent[j++];
            }
        }
        while (i < leftSize) {
            all[k++] = leftStudent[i++];
        }
        while (j < rightSize) {
            all[k++] = rightStudent[j++];
        }
    }
}
