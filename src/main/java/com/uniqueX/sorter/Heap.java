package com.uniqueX.sorter;

import com.uniqueX.Entities.Student;
import com.uniqueX.ISorter;

import java.util.Arrays;

public class Heap implements ISorter {

    @Override
    public void sortStudents(Student[] students) {
        System.out.println("Heap Sort");
        System.out.println(Arrays.toString(students));

        for (int i = students.length / 2 - 1; i >= 0; i--) {
            heapify(students, students.length, i);
        }

        for (int i = students.length - 1; i > 0; i--) {
            Student tempStudent = students[0];
            students[0] = students[i];
            students[i] = tempStudent;
            heapify(students, i, 0);
        }
        System.out.println(Arrays.toString(students));
    }


    private void heapify(Student arr[], int n, int i) {
        int largest = i;
        int leftChild = 2 * i + 1;
        int rightChild = 2 * i + 2;

        if (leftChild < n && arr[leftChild].getScore() > arr[largest].getScore()) {
            largest = leftChild;
        }

        if (rightChild < n && arr[rightChild].getScore() > arr[largest].getScore()) {
            largest = rightChild;
        }

        if (largest != i) {
            Student tempStudent = arr[i];
            arr[i] = arr[largest];
            arr[largest] = tempStudent;
            heapify(arr, n, largest);
        }
    }
}
